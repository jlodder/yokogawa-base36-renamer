# yokogawa-base36-renamer

Yokogawa DL750 Scopecorder script to rename 6.3 filenames with base36 timestamps.

## How it works

Change path variable at [line 6](https://gitlab.com/jlodder/yokogawa-base36-renamer/-/blob/master/yokogawa%20renamer.py#L6)  
Run script.  

`JBE90EX8.HDR`  -> `JBE90EX8 - 2019-11-14T09-32-14.HDR` 

