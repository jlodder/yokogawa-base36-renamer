import os
import glob
import time

# Path to scan for files (must end with \\)
path = 'sample\\'

# Build list of files
filelist = []
for root, dirs, files in os.walk(path):
    for file in files:
        # Take these extensions
        if file.endswith('.HDR'):
            filelist.append(file)
        if file.endswith('.WVF'):
            filelist.append(file)
        if file.endswith('.SET'):
            filelist.append(file)

count = 0

# For each file
for f in filelist:
    # Not renamed before
    if len(f) < 13:
        # Decode base36
        year = int(f[0:1], 36) + 2000
        month = int(f[1:2], 36)
        day = int(f[2:3], 36)
        hour = int(f[3:4], 36)
        time = int(f[4:8], 36) * 100
        minute = int(time/60000)
        seconds = int((time - (minute * 60000)) / 1000)
        # Compile new name
        timestamp = '{:04d}-{:02d}-{:02d}T{:02d}-{:02d}-{:02d}'.format(year,month,day,hour,minute,seconds)
        debugstamp = '{} = {:04d}-{:02d}-{:02d}T{:02d}-{:02d}-{:02d}'.format(f,year,month,day,hour,minute,seconds)
        print(debugstamp)
        new = f[:8]+ ' - ' + str(timestamp) + f[8:12]
        print('new: '+new)
        os.rename(path+f,path+new)
        count += 1

print('Renamed {} files. Return to DOS name with original string (kept) for yokogawa to read'.format(count))
